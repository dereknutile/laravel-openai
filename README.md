# Laravel OpenAI Example

## Development

### OpenAI API Key
Obtain an OpenAI API Key here: [https://beta.openai.com/signup](https://beta.openai.com/signup).

### Clone this project
    $ git clone https://gitlab.com/dereknutile/laravel-openai.git
    $ cd laravel-openai
    $ composer update
    $ npm update

### Set the API Key
Edit the `.env` file and add the following using the OpenAI API Key you obtained:

    OPENAI_API_KEY="openai_api_key_you_obtained"

### Start the application

    $ php artisan serve

Browse to [http://localhost:8000](http://localhost:8000).
