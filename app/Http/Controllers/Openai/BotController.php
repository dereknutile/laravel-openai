<?php

namespace App\Http\Controllers\Openai;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use OpenAI\Laravel\Facades\OpenAI;

class BotController extends Controller
{
    public function respondToRequest(Request $request)
    {
        $request->validate([
            'prompt' => ['required','string','max:1024']
        ]);

        $answer = OpenAI::completions()->create([
            'model' => 'text-davinci-003',
            'prompt' => $request->prompt,
            'temperature' => 0.9,
            'max_tokens' => 150,
            'frequency_penalty' => 0,
            'presence_penalty' => 0.6,
        ]);

        // Return the response
        return response()->json([
            'response' => $answer->choices[0]->text
        ]);
    }
}
