<form method="POST" action="{{route('ask')}}">
    @csrf
    <label for="prompt">Prompt:</label><br>
    <textarea name="prompt" rows="4" cols="50">PHP is</textarea><br>
    <input type="submit" value="Submit">
</form>
<br>
<div id="response">
    @if (isset($response))
        {{ $response }}
    @endif
</div>
